require 'test_helper'

class CommentsControllerTest < ActionController::TestCase
  setup do
    @post     = posts(:one)
    @comment  = comments(:one)
  end

  test "should get index" do
    get :index, post_id: @post.id
    assert_response :success
    assert_not_nil assigns(:comments)
  end

  test "should get new" do
    get :new, post_id: @post.id
    assert_response :success
  end

  test "should create comment" do
    assert_difference('Comment.count') do
      post :create, post_id: @post.id, comment: { body: @comment.body, post_id: @comment.post_id }
    end

    assert_redirected_to assigns(:post)
  end

  test "should show comment" do
    get :show, post_id: @post.id, id: @comment
    assert_response :success
  end

  test "should get edit" do
    get :edit, post_id: @post.id, id: @comment
    assert_response :success
  end

  test "should update comment" do
    patch :update, post_id: @post.id, id: @comment, comment: { body: @comment.body, post_id: @comment.post_id }
    assert_redirected_to assigns(:post)
  end

  test "should destroy comment" do
    assert_difference('Comment.count', -1) do
      delete :destroy, post_id: @post.id, id: @comment
    end

    assert_redirected_to assigns(:post)
  end
end
